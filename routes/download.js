var express = require('express');
var router = express.Router();

var redis = require('redis');
client = redis.createClient({return_buffers: true});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

router.get('/:id/:name', function (req, res, next) {
    client.get("files:" + req.params.id, function (err, value) {
        if (err) return next(err);

        if (!value) {
            next();
        } else {
            var newFileName = encodeURIComponent(req.params.name);
            res.setHeader('Content-Type', 'multipart/form-data');
            res.setHeader("Content-Disposition", "attachment;filename=" + newFileName);
            res.end(value); //send the value and end the connection
        }

    });
});

module.exports = router;