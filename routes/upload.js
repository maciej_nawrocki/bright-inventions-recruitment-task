var express = require('express');
var router = express.Router();
var Busboy = require('busboy');
var randomstring = require("randomstring");

var crypto = require('crypto');

var redis = require('redis');
client = redis.createClient({return_buffers: true});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

router.get('/', function (req, res, next) {
    res.render('upload', {title: 'File upload'});
});

router.post('/file_upload',
    function (req, res, next) {
        var busboy = new Busboy({headers: req.headers});
        var fileData;
        var hash = crypto.createHash('md5');
        hash.setEncoding("hex");

        busboy.on('file', function (fieldname, file, filename) {
            //the data event of the stream
            file.on('data', function (data) {
                if (!fileData) {
                    fileData = data;
                } else {
                    fileData = Buffer.concat([fileData, data]);
                }
            });
            //when the stream is done
            file.on('end', function () {
                hash.write(fileData.toString());
                hash.end();
                var fileId = randomstring.generate(20);
                var timestamp = getDate();
                var uploadedObject = {
                    name: filename,
                    hash: hash.read(),
                    date: timestamp,
                    fileId: fileId
                }
                client.hmset(
                    "data:" + randomstring.generate(20),
                    uploadedObject,
                    function (err) {
                        if (err) return next(err);
                    }
                );
                client.set(
                    "files:" + fileId,
                    new Buffer(fileData, 'base64'),
                    function (err) {
                        if (err) return next(err);

                        res.render('upload', {title: 'File upload', response: "File uploaded correctly"});
                    }
                );
            });
        });
        req.pipe(busboy);
    }
);

function getDate() {
    var currentDate = new Date();
    return currentDate.getDate() + "-"
        + (currentDate.getMonth() + 1) + "-"
        + currentDate.getFullYear() + " "
        + currentDate.getHours() + ":"
        + currentDate.getMinutes() + ":"
        + currentDate.getSeconds() + ":"
        + currentDate.getMilliseconds();
}

module.exports = router;