var express = require('express');
var router = express.Router();
var async = require('async');

var redis = require('redis');
client = redis.createClient({return_buffers: true});

client.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

router.get('/', function (req, res, next) {
    client.keys('data:*', function (err, keys) {
        if (err) return next(err);

        if (keys) {
            async.map(keys, function (key, cb) {
                client.hgetall(key, function (err, value) {
                    if (err) return next(err);

                    var fileInfo = {};
                    fileInfo['id'] = key.toString();
                    fileInfo['name'] = value.name.toString();
                    fileInfo['hash'] = value.hash.toString();
                    fileInfo['date'] = value.date.toString();
                    fileInfo['fileId'] = value.fileId.toString();
                    cb(null, fileInfo);

                });
            }, function (err, results) {
                if (err) return next(err);

                res.render('list', {title: 'List of files', filesInfo: results});
            });
        }
    });
});

module.exports = router;