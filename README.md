# Bright inventions recruitment task

Node.js web application for uploading files and saving their hashes in Redis database.

## Getting started

To download application via git paste this to commandline:

git clone https://maciej_nawrocki@bitbucket.org/maciej_nawrocki/bright-inventions-recruitment-task.git

### Prerequisites

Before running application, install and run redis database and install node.js(with npm).

Optionally you can install Redis Desktop Manager which should help you navigate your current DB’s.

node.js:
https://nodejs.org/en/

redis:
https://redis.io/download

Redis Desktop Manager:
https://redisdesktop.com/

Tutorial - how to run node.js with redis:
https://codeburst.io/introduction-to-redis-node-js-demo-f3326dd43c0f

## Built With

#### nmp

Navigate to inside the project folder on terminal.

Do an *npm install* for installing all the project dependencies.

Then *node app.js* to get the app running on local host.

#### WebStorm

Open project.

Right click on the *app.js* file, select *Run 'app.js'*

## Authors

* **Maciej Nawrocki** - *Initial work* - [Maciej Nawrocki](https://bitbucket.org/maciej_nawrocki/)